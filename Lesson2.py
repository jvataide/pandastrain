import matplotlib.pyplot as plt
import pandas as pd #this is how I usually import pandas
import random
import os

names = ['Bob','Jessica','Mary','John','Mel']

random.seed(500)

random_names = [names[random.randint(0,len(names)-1)] for i in range(1000)]

births = [random.randint(0,999) for i in range(1000)]

BabyDataSet = list(zip(random_names,births))

df = pd.DataFrame(data = BabyDataSet, columns=['Names', 'Births'])

df.to_csv('births1880.txt', index=False, header=False)

df = pd.read_csv('births1880.txt', header=None, names=['Names', 'Births'])

os.remove('births1880.txt')
#print(df)
#print(df.info())
#print(df.head())
#print(df['Names'].unique())
print(df['Names'].describe())

name = df.groupby('Names')
df2 = name.sum()

#print(df2)
plt.plot(df2['Births'])
print(df2.sort_values(by='Births', ascending=False))
plt.bar(10, 100000)
#Falha mizerável em fazer gráficos em barra, retornar depois para não perder tempo
plt.show(block=True)