import pandas as pd


d = {'one':[1,1],'two':[2,2]}
i = ['a','b']

df = pd.DataFrame(data=d, index=i)

print(df)

stack = df.stack()
print(stack)
print(stack.index)

unstack = df.unstack()
print(unstack.index)
print(unstack)

transpose = df.T
print(transpose)

print(transpose.index)

print(stack.unstack())