import matplotlib.pyplot as plt
import pandas as pd #this is how I usually import pandas
import os

# The inital set of baby names and birth rates
names = ['Bob','Jessica','Mary','John','Mel']
births = [968, 155, 77, 578, 973]

BabyDataSet = list(zip(names,births))

df = pd.DataFrame(data = BabyDataSet, columns=['Names', 'Births'])

df.to_csv('births1880.csv',index=False,header=True)

Location = 'births1880.csv'

df = pd.read_csv(Location)

print(df)
os.remove(Location)

print(df['Births'].max())

print(df['Names'][df['Births'] == df['Births'].max()])

plt.plot(df['Births'])

MaxValue = df['Births'].max()

MaxName = df['Names'][df['Births'] == df['Births'].max()].values

Text = str(MaxValue) + " - " + MaxName

plt.annotate(Text, xy=(1, MaxValue), xytext=(8, 0), xycoords=('axes fraction', 'data'), textcoords='offset points')

print("The most popular name")
print(df[df['Births'] == df['Births'].max()])

plt.show(block=True)
