import matplotlib.pyplot as plt
import pandas as pd #this is how I usually import pandas
import numpy as np
import os

np.random.seed(111)


def CreateDataSet(Number=1):
    Output = []

    for i in range(Number):
        rng = pd.date_range(start='1/1/2009', end='12/31/2012', freq='W-MON')

        data = np.random.randint(low=25, high=1000, size=len(rng))

        status = [1, 2, 3]

        random_status = [status[np.random.randint(low=0, high=len(status))] for i in range(len(rng))]

        states = ['GA', 'FL', 'fl', 'NY', 'NJ', 'TX']

        random_states = [states[np.random.randint(low=0, high=len(states))] for i in range(len(rng))]

        Output.extend(zip(random_states, random_status, data, rng))

    return Output

#print(CreateDataSet()[0:3])
#print(CreateDataSet(Number=2)[209:212])



dataset = CreateDataSet(4)
df = pd.DataFrame(data=dataset, columns=['State','Status','CustomerCount','StatusDate'])
print(df.head())

df.to_excel('Lesson3.xlsx', index=False)

df = pd.read_excel('Lesson3.xlsx', 0, index_col='StatusDate')

print(df.head())

df['State'] = df.State.apply(lambda x: x.upper())

mask = df['Status'] == 1
df=df[mask]
print(mask)
mask = df.State == 'NJ'
df['State'][mask] = 'NY'

print(df['State'].unique())

#plt.plot(df['CustomerCount'])
#plt.show(block=True)
#???????????????


Weekly = df.reset_index().groupby(['State','StatusDate']).sum()
del Weekly['Status']
#print(Weekly.head())

#print(Weekly.index.levels[0])



plt.plot(Weekly.loc['FL'])
plt.plot(Weekly.loc['GA'])
plt.plot(Weekly.loc['NY'])
plt.plot(Weekly.loc['TX'])

plt.plot(Weekly.loc['FL']['2012':])
plt.plot(Weekly.loc['GA']['2012':])
plt.plot(Weekly.loc['NY']['2012':])
plt.plot(Weekly.loc['TX']['2012':])
#Plotar tudo é loucura... aprender pyplot bem antes



# Calculate Outliers
StateYearMonth = Weekly.groupby([Weekly.index.get_level_values(0), Weekly.index.get_level_values(1).year, Weekly.index.get_level_values(1).month])
Weekly['Lower'] = StateYearMonth['CustomerCount'].transform( lambda x: x.quantile(q=.25) - (1.5*x.quantile(q=.75)-x.quantile(q=.25)) )
Weekly['Upper'] = StateYearMonth['CustomerCount'].transform( lambda x: x.quantile(q=.75) + (1.5*x.quantile(q=.75)-x.quantile(q=.25)) )
Weekly['Outlier'] = (Weekly['CustomerCount'] < Weekly['Lower']) | (Weekly['CustomerCount'] > Weekly['Upper'])

# Remove Outliers
Weekly = Weekly[Weekly['Outlier'] == False]


x = range(10)
y = range(10)

fig, ax = plt.subplots(nrows=4, ncols=1)

for row in ax:
        row.plot(x, y)

plt.show()

