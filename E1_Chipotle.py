import matplotlib.pyplot as plt
import pandas as pd
import os


data = pd.read_csv('https://raw.githubusercontent.com/justmarkham/DAT8/master/data/chipotle.tsv',sep='\t')
chipo = pd.DataFrame(data)

print(chipo.head(10))

print(chipo.info(), chipo.shape[0],  len(chipo))

print(chipo.shape[1])

print(chipo.columns)

print(chipo.index)