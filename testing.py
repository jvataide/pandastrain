import pandas as pd
import numpy as np

A = ['a', 'c', 'c']
B = ['b', 'd', 'b']
C = [1, 3, 5]

df = pd.DataFrame(np.array([A, B, C]).T, columns=['A', 'B', 'C'], index=['X', 'Y', 'Z'])


df.columns = ['A', 'B', 'C']
df['C'] = df['C'].apply(pd.to_numeric)
print(df)

print(df['C'])
newdf = df.groupby('B')

print(newdf)
"""
print(newdf.sum())

print(df['C'])
df.index.name = 'IDK'
print(df)
df = df.reset_index()
print(df)
df.index = df['IDK']
print(df)
"""
print(newdf['C'].transform(sum))
df['SBS'] = newdf['C'].transform(lambda x: x.sum())

print(df)
